2021 Sep 21

This conversation is loosely transcrabed in the moment.

This person works in computational biology, in protein folding simulations. They have a strong CS and biology background.

Sam: What kind of code do you work on?
Interviewee: Mostly scripting to string together other programs.
Interviewee: The other half is data analysis, which is pandas.
Sam: How do you make sure your intermediate results are up to date?
Interviewee: The proper way would be VCS, but I have an established protocol to generate the data. I store both the intermediates and the raw output.
Sam: How to invalidate?
Interviewee: I keep the run files, so I manually move the intermediates around.
Sam: So it's driven by the filesystem organization?
Interviewee: In the output directory, there's also a copy of the input.
Sam: That's kind of how used to I do things too.
Sam: What kind of langugaes do you work on?
Interviewee:The simulator is written in C++, but I'm just generating XML inputs to the simulator. I query the sqlite output. It's not high intensity, but there are 21k sqlite databases I'm trying to read and aggregate.
Sam: And you're running on commodity desktop?
Interviewee: For development, I'm using a commodity desktop hardware or lightweight compute nodes.
Sam: I'm surprised your desktop can handle that.
Interviewee: It generates this data structure in memory for 30 minutes in RAM and then outputs the optimum into DB.
Sam: Is the optimization search in parallel?
Interviewee: No, but it can be searching multiple spaces in parallel.
Sam: Is the runtime of the code a bottleneck for doing basic research you can do (e.g. if it could run twice as fast)?
Interviewee: My old code would take 3 -- 4 days, and the thing I wanted to try would take 70 days.
Sam: Tell me about the available nodes you can use?
Interviewee: UTSW has an on-premise bare-metal SLURM cluster, where nodes have 16 cores 32 Gb RAM up to some big cores with GPU.
Sam: So then how do you package your software environment?
Interviewee: Originally, I just managed dependencies manually, but when I started helping other people run the same thing, I started packaging a conda environment (Rosetta is already on every node). Mostly, I depend on Pandas and BioPython. Ideally, I guess I would write a Singularity container, but Rosetta is 30 Gb.
Note: Rosetta modelling software: https://www.rosettacommons.org/
Interviewee: All this analysis stuff is not performance intensive; it's the simulator that is.

Sam: What software virtues in research software matter to you?
Interviewee: For research infra: portability (dependencies, tight coupling to machine state), high-level documentation, high-level structure, modularity. For about per-project code: readability/auditability, portability-lite, extensibility-lite.
Sam: Readability => correctness.
Interviewee: Peer review doesn't take readability of software into account. There's a lot of computational fields that wouldn't be good at reviewing code.
Sam: Computational biology have artifact badges?
Interviewee: No. For infrastructure and methods papers, that's something I think people would want.

Sam: How does one achieve these software virtues?
Interviewee: Low-level: version control, dependency management (easy to use). High-level: extensibility and structure need a lot of thinking from a software engineering perspective. High-level: well-documentated.
Sam: What about code review?
Interviewee: It's really down to the quality of the reviewer. If the code review happens all at the end, it could delay everything.
Sam: Ideally, review in small chunks.
Interviewee: But the org might be too small. Also there is a high domain-knowledge. For research infra with a large developer base, there is definitely code review.
Sam: How important is testing for software virtues?
Interviewee: For infrastructure code, you should have tests to make sure you code is correct. For things like data analysis, you don't need testing.
Sam: Have you ever made a commit to the data analysis which you later found was broken?
Interviewee: No, I'm doing mostly simple operations.
Sam: What about running the whole pipeline on a fresh set of data? I think.
Interviewee: I had something like that I did by hand.
