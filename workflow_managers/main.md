---
title: Improving Software Engineering in Academia
author: Samuel Grayson
standalone: true
css: main.css
---

|Name|Domain|Distributed|Task overhead|Data parallel|Provenance|Dependency-time|Delete unused products|Correct wrt changes in dependencies|Memory-aware scheduling|
|--|--|--|--|--|--|--|--|--|--|
|[funsies](https://github.com/aspuru-guzik-group/funsies)||Distributed|
|[Luigi](https://luigi.readthedocs.io/en/stable/index.html)||Distributed|
|[Reena](https://docs.reana.io/)|Computational science||Distributed|
|[Yadage](https://yadage.readthedocs.io/en/latest/definingworkflows.html)|Computational science|Distributed|
|[Snakemake](https://snakemake.github.io/)|Computational science|Distributed|
|[Popper](https://getpopper.io/)|Computational science|
|[Nix](https://nixos.org/)|Package management|Distributed|
|[Bazel](https://bazel.build/)|Build system|Distributed|
|[Buck](https://buck.build/)|Build system|Single server|
|[Make](https://www.gnu.org/software/make/)|Build system|Single server|
|[Scons](https://scons.org/)|Build system|Single server|
|[Rake](https://martinfowler.com/articles/rake.html)|Build System|Single server|
|[Shake](https://shakebuild.com/)|BuildSystem|Single server|

- [CWL](https://www.commonwl.org/user_guide/index.html): Just a language for going to other workflow managers.

- [Apache Flink](https://flink.apache.org/): Stream oriented.

- [Apache Airflow](https://airflow.apache.org/): Too complex, designed for big-data.

IncPy is not embeddable in server applications.

Detect impure writes
