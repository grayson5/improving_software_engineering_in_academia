#!/bin/sh

source $stdenv/setup
mkdir $out
cd $src

extensions="yaml_metadata_block+citations+pipe_tables"
pandoc main.md --from=markdown+${extensions} --css=main.css --output=$out/main.html
