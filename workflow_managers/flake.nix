{
  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
  };
  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
        name = "document";
      in
      {
        packages.${name} = pkgs.stdenv.mkDerivation {
          inherit name;
          src = ./.;
          builder = ./builder.sh;
          buildInputs = [
            pkgs.pandoc
            (
              pkgs.texlive.combine {
                inherit (pkgs.texlive)
                  scheme-basic
                  savetrees
                  xcolor
                  xkeyval
                  microtype
                  booktabs
                  etoolbox
                  mdwtools
                ;
              }
            )
          ];
        };
        defaultPackage = self.packages.${system}.${name};
      });
}
