{
  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
  };
  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
        name = "document";
      in
      {
        packages.${name} = pkgs.stdenv.mkDerivation {
          inherit name;
          src = ./.;
          builder = ./builder.sh;
          buildInputs = [
            pkgs.pandoc
            pkgs.librsvg
            pkgs.inkscape
            (
              pkgs.texlive.combine {
                inherit (pkgs.texlive)
                  scheme-basic
                  savetrees
                  xcolor
                  xkeyval
                  titlesec
                  microtype
                  booktabs
                  etoolbox
                  mdwtools
                  svg
                  koma-script
                  trimspaces
                  transparent
                  pgf
                ;
              }
            )
          ];
        };
        defaultPackage = self.packages.${system}.${name};
      });
}
