references:
- type: article
  id: collberg
  author:
  - given: Christian
    family: Collberg
  - given: Todd A.
    family: Proebsting
  title: Repeatability in computer systems research
  issued: 2016-03
  published: 2016-02-26
  publisher: Association for Computing Machinery
  volume: 59
  issue: 3
  ISSN: 0001-0782
  URL: https://dl.acm.org/doi/10.1145/2812803
  DOI: 10.1145/2812803
  container-title: Communications of the ACM
  language: en
  page: 62-69
  number-of-pages: 8

- type: manuscript
  id: krishnamurthi
  title: 'Examining "Reproducibility in Computer Science"'
  author:
    - given: Shriram
      family: Krishnamurthi
  url: http://cs.brown.edu/~sk/Memos/Examining-Reproducibility/

- type: article
  id: rosene
  author:
  - given: A. F.
    family: Rosene
  - given: J. E.
    family: Connolly
  - given: K. M.
    family: Bracy
  container-title: IEEE Transactions on Reliability
  title: Software Maintainability --- What It Means and How to Achieve It
  published: 1981-08
  volume: R-30
  number: 3
  pages: 240-245
  DOI: 10.1109/TR.1981.5221065
  URL: https://ieeexplore.ieee.org/document/5221065

- type: article
  id: vandewalle
  author:
    - given: Patrick
      family: Vandewalle
    - given: Jelena
      family: Kovacevic
    - given: Martin
      family: Vetterli
  container-title: IEEE Singal Processing Magazine
  published: 2009
  volume: 26
  number: 3
  pages: 37-47
  doi: 10.1109/MSP.2009.932122
  url: https://ieeexplore.ieee.org/document/4815541

- type: article
  id: merton
  author:
  - given: Robert K.
    family: Merton
  title: Science and technology in a democratic order
  container-title: Journal of Legal and Political Sociology
  published: 1942
  volume: 1
  issue: 1
  pages: 115-126

- type: article
  id: mccabe
  author:
  - given: T. J.
    family: McCabe
  container-title: IEEE Transactions on Software Engineering
  title: A Complexity Measure
  published: 1976
  volume: SE-2
  number: 4
  pages: 308-320
  DOI: 10.1109/TSE.1976.233837
  URL: https://ieeexplore.ieee.org/document/1702388

- type: article
  id: static-analysis
  author:
    - family: Johnson
      given: Brittany
    - family: Song
      given: Yoonki
    - family: Murphy-Hill
      given: Emerson
    - family: Bowdidge
      given: Robert
  container-title: 2013 35th International Conference on Software Engineering (ICSE)
  title: Why don't software developers use static analysis tools to find bugs?
  published: 2013
  pages: 672-681
  DOI: 10.1109/ICSE.2013.6606613
  URL: https://ieeexplore.ieee.org/abstract/document/6606613

- type: manuscript
  id: eisty
  author:
  - given: Nasir U.
    family: Eisty
  - given: Jeffrey C.
    family: Carver
  url: https://arxiv.org/abs/2109.10971
  title: Developers Perception of Peer Code Review in Research Software Development

- type: article
  id: neupane
  title: Characterization of Leptazolines A--D, Polar Oxazolines from the Cyanobacterium _Leptolyngbya_ sp., Reveals a Glitch with the "Willoughby--Hoye" Scripts for Calculating NMR Chemical Shifts
  container-title: Organic Letters
  volume: 21
  number: 20
  pages: 8449-8453
  published: 2019
  DOI: 10.1021/acs.orglett.9b03216
  URL: https://pubs.acs.org/doi/10.1021/acs.orglett.9b03216
  author:
  - given: Jayanti Bhandari
    family: Neupane
  - given: Ram P.
    family: Neupane
  - given: Yuheng
    family: Luo
  - given: Wesley Y.
    family: Yoshida
  - given: Rui
    family: Sun
  - given: Phillip G.
    family: Williams

- type: article
  id: willoughby
  published: 2014-02-20
  container-title: Nature Protocols
  volume: 9
  issue: 3
  pages: 643-660
  DOI: https://doi.org/10.1038/nprot.2014.042
  URL: https://www.nature.com/articles/nprot.2014.042
  author:
  - given: Patrick H.
    family: Willoughby
  - given: Matthew J.
    family: Jansma
  - given: Thomas R.
    family: Hoye
  title: A guide to small-molecule structure assignment through computation of (<sup>1</sup>H and <sup>13</sup>C) NMR chemical shifts

- title: A journey of reproducibility from Excel to Pandas
  type: post-weblog
  id: hettrick-excel
  publisher: Software Sustinability Institute
  URL: https://www.software.ac.uk/blog/2017-09-06-journey-reproducibility-excel-pandas
  author:
    - given: Simon
      family: Hettrick
  published: 2017-08-06

- title: UK Research Software Survey 2014
  type: dataset
  id: hettrick
  publisher: Zenodo
  DOI: 10.5281/zenodo.14809
  author:
  - given: Simon
    family: Hettrick
  - given: Mario
    family: Antonioletti
  - given: Les
    family: Carr
  - given: Neil
    family: Chue Hong
  - given: Stephen
    family: Crouch
  - given: David
    family: De Roure
  - given: Iain
    family: Emsley
  - given: Carole
    family: Goble
  - given: Alexander
    family: Hay
  - given: Devasena
    family: Inupakutika
  - given: Mike
    family: Jackson
  - given: Aleksandra
    family: Nenadic
  - given: Tim
    family: Parkinson
  - given: Mark I
    family: Parsons
  - given: Aleksandra
    family: Pawlik
  - given: Giacomo
    family: Peru
  - given: Arno
    family: Proeme
  - given: John
    family: Robinson
  - given: Shoaib
    family: Sufi
  issued:
    date-parts:
    - - 2014
      - 12
      - 4
