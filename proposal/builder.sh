#!/bin/sh

source $stdenv/setup
mkdir $out
cd $src

extensions="yaml_metadata_block+citations"
pandoc proposal.md --from=markdown+${extensions} --citeproc --output=$out/proposal.pdf
