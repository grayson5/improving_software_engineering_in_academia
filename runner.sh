docker run \
	   -v /srv/gitlab-runner/config:/etc/gitlab-runner \
	   -v /var/run/docker.sock:/var/run/docker.sock \
	   -it \
	   gitlab/gitlab-runner
