This repo holds writing and code related to Sam's project to improve software engineering in academia.

To build the proposal, install [Nix][nix] and run

```
cd proposal
nix-build
xdg-open result/proposal.pdf
```

[nix]: https://nixos.org/
