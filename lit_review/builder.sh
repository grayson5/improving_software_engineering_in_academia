#!/bin/sh

source $stdenv/setup
mkdir $out
cd $src

extensions="yaml_metadata_block+citations"
pandoc main.md --from=markdown+${extensions} --citeproc --output=$out/main.pdf
