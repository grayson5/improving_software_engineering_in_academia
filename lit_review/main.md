---
title: Improving Software Engineering in Academia
author: Samuel Grayson
csl: acm
bibliography: bib.yaml
citeproc: true
standalone: true
header-includes:
  - \usepackage[subtle]{savetrees}
  - \hypersetup{colorlinks=true}
---

# Related studies

Eisty and Carver [@eisty] study "What is the effect of peer code review on research software engineering? What are common positive and negative experiences? What are difficulties and barriers? What improvements can be made?". They found its practitioners believe that its effect is to improve code quality and facilitate knowledge sharing. These effects are also "positive experiences." Negative experiences include review taking too long and developers misunderstanding the feedback. Difficulties include the challenge of understanding the code, while barriers include finding the time to do reviews. Potential improvements include formalizing process, using more software tools, and funding more people to do reviews.

Collberg and Proebsting [@collberg] study "How much of published computer systems research in 2012 is repeatable?". The researchers found that roughly 2/3 had source code, and of those, about 1/2 were repeatable out-of-the-box. The results has been questioned in follow up work by Krishnamurthi [@krishnamurthi] because perhaps those tasked with reproducing were not knowledgeable enough.

Vandewalle et al. [@vandewalle] study "Does releasing source code correlate with increased citation-count for image processing in 2004?". The researchers found a correlation, but leave causality to future work. About 1/10 of their samples had source code in image processing journals in 2004; this is strikingly different from Collberg [@collberg], perhaps because the domain and time were different.

Murphy-Hill et al. [@murphy-hill] study "What makes software developers productive in 2019?". They found that the most important are: job enthusiasm, peer support for new ideas, useful feedback about job performance.

# Software Development Practices

- **Peer code review**: studied by [@eisty]

# Methods

Eisty and Carver [@eisty] use survey to study how a process works, what are the positives, and what are the negatives. They do a pilot study first. They never directly assess efficacy of that process, just "do the practitioners find it important?".

Collberg [@collberg] have undergraduate researchers attempt to repeat research experiments. Krishnamurthi [@krishnamurthi] shows that they may not be knowledgeable enough and erroneously declare software not repeatable.

Vandewalle [@vandewalle] uses citation count to measure research impact and searches for source-code by hand.

Murphy-Hill [@murphy-hill] uses surveys to quantify environmental variables and self-assessment to quantify productivity.
