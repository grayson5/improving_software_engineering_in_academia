import asyncio
import typer
import pandas as pd
from pathlib import Path
from dataclasses import asdict
from .test_cache import test_cache


ROOT = Path(__file__).parent.parent


def main():
    repo_results = asyncio.run(test_cache())
    pd.DataFrame.from_records(
        [
            {
                "commit": commit_result.commit,
                "orig_time": commit_result.orig_time,
                "orig_success": commit_result.orig_success,
                "memo_time": commit_result.memo_time,
                "memo_success": commit_result.memo_success,
                "stdout_match": commit_result.stdout_match,
                "index_read": commit_result.index_read,
                "index_write": commit_result.index_write,
                "name": repo.name,
                "commit_no": commit_no,
                "functions": 0,
            }
            for repo, commit_results in repo_results
            for commit_no, commit_result in enumerate(commit_results)
        ],
        index=("name", "commit_no")
    ).to_csv(ROOT / "results.csv")
    pd.DataFrame.from_records(
        [
            {
                **asdict(function_data),
                "name": repo.name,
                "function": function,
            }
            for repo, commit_results in repo_results
            for commit_result in commit_results
            for function, function_data in commit_result.functions.items()
        ],
    ).groupby(by=["name", "function"]).sum().to_csv(ROOT / "overheads.csv")

typer.run(main)
