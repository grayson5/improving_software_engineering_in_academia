import asyncio
import collections
import datetime
from dataclasses import dataclass, field
import json
from pathlib import Path
import shutil
from typing import Callable, List, Tuple, TypeVar, Mapping

from .data import Action, Repo, repos

from charmonium.cache import memoize, MemoizedGroup, DirObjStore
import charmonium.cache


ROOT = Path(__file__).parent.parent


Return = TypeVar("Return")


group = MemoizedGroup(obj_store=DirObjStore(ROOT / ".cache/functions"))

import logging
hash_logger = logging.getLogger("charmonium.cache.determ_hash")
hash_logger.setLevel(logging.DEBUG)
hash_logger.addHandler(logging.FileHandler("/tmp/hash.log"))
hash_logger.propagate = False

@dataclass
class CachedFunctionPerf:
    outer_function: float = 0
    hash: float = 0
    misses: int = 0
    hits: int = 0
    inner_function: float = 0
    serialize: float = 0
    obj_store: float = 0
    obj_load: float = 0
    deserialize: float = 0

@dataclass
class CommitResult:
    commit: str
    orig_time: float
    orig_success: bool
    memo_time: float
    memo_success: bool
    memo2_time: float
    memo2_success: bool
    stdout_match: bool
    stdout_match2: bool
    index_read: float = 0
    index_write: float = 0
    functions: Mapping[str, CachedFunctionPerf] = field(default_factory=lambda: collections.defaultdict(CachedFunctionPerf))

def timeit(thunk: Callable[[], Return]) -> Tuple[datetime.timedelta, Return]:
    start = datetime.datetime.now()
    ret = thunk()
    stop = datetime.datetime.now()
    return (stop - start, ret)


@memoize(group=group)
def get_commit_result(commit: str, repo: Repo, action: Action) -> CommitResult:
    from tqdm import tqdm
    repo.checkout(commit)
    log_path = Path("/tmp/logs")
    if log_path.exists():
        shutil.rmtree(log_path)
    log_path.mkdir()
    orig_time, (orig_stdout, orig_success) = timeit(
        lambda: action.run(repo=repo)
    )
    memo_time, (memo_stdout, memo_success) = timeit(
        lambda: action.run(repo=repo, env_override={
            "CHARMONIUM_CACHE": "enable",
            "CHARMONIUM_CACHE_PERF_LOG": str(log_path / "perf.log"),
        })
    )
    memo2_time, (memo2_stdout, memo2_success) = timeit(
        lambda: action.run(repo=repo, env_override={
            "CHARMONIUM_CACHE": "enable",
            "CHARMONIUM_CACHE_PERF_LOG": str(log_path / "perf.log"),
        })
    )
    result = CommitResult(
        commit=commit,
        orig_time=orig_time.total_seconds(),
        orig_success=orig_success,
        memo_time=memo_time.total_seconds(),
        memo_success=memo_success,
        memo2_time=memo_time.total_seconds(),
        memo2_success=memo_success,
        stdout_match=orig_stdout == memo_stdout,
        stdout_match2=orig_stdout == memo2_stdout,
    )
    with (log_path / "perf.log").open() as perf_log:
        for line in perf_log:
            record = json.loads(line)
            if "name" in record:
                result.functions[record["name"]].__dict__[record["event"]] += record["duration"]
                if record["event"] == "outer_function":
                    result.functions[record["name"]].hits += int(record["hit"])
                    result.functions[record["name"]].misses += int(not record["hit"])
            else:
                result.__dict__[record["event"]] += record["duration"]

    return result


@memoize(group=group)
def get_repo_result(repo: Repo, action: Action) -> List[CommitResult]:
    from tqdm import tqdm
    commits = repo.get_commits()
    return [get_commit_result(commit, repo, action) for commit in tqdm(commits, total=len(commits), desc="Testing commit")]


async def test_cache() -> List[Tuple[Repo, List[CommitResult]]]:
    from tqdm import tqdm
    await asyncio.gather(*[repo.setup() for repo, _ in tqdm(repos, total=len(repos), desc="Repo setup")])
    await asyncio.gather(*[action.setup(repo) for repo, action in tqdm(repos, total=len(repos), desc="Action setup")])
    return [(repo, get_repo_result(repo, action)) for repo, action in tqdm(repos, total=len(repos), desc="Testing repo")]
